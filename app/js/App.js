import React from 'react';
import Router from 'react-router';
import Reflux from 'reflux';
import Header from './nav/Header.js';
import Footer from './nav/Footer.js';
import SideMenu from './nav/SideMenu.js';

let App = React.createClass({
    mixins: [Reflux.ListenerMixin],

    onMenuOpen() {
        let el = React.findDOMNode(this.refs.wrapper);
        el.style.overflow = 'hidden';
    },

    onMenuClose() {
        let el = React.findDOMNode(this.refs.wrapper);
        el.style.overflow = 'auto';
    },

    render() {
        // let footer = this.state.loaded ?  : false;

        return (
            /* jshint ignore:start */
            <div ref="wrapper" className="wrapper">
                <Header page={this.props.data.id} />
                <Router.RouteHandler data={this.props.data} />
                <Footer />
                <SideMenu />
            </div>
            /* jshint ignore:end */
        );
    }
});

export default App;
