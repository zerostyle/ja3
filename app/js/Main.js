import React from 'react';
import Router from 'react-router';
import App from './App.js';
import BasePath from './utils/path.js';
import Home from './pages/Home.js';
import Information from './pages/Information.js';
import Projects from './pages/Projects.js';
import Project from './pages/Project.js';
import ProjectDetail from './pages/ProjectDetail.js';

let routes = (
    /* jshint ignore:start */
    <Router.Route name="app" path="/" handler={App}>
        <Router.Route name="home" path="/" handler={Home}/>

        <Router.Route name="projects" path="/set/projects" handler={Projects}/>
        <Router.Route name="projectsList" path="/set/projects/:id" handler={Project}/>
        <Router.Route name="projectsDetail" path="/set/projects/:id/:num" handler={ProjectDetail}/>

        <Router.Route name="images" path="/set/images" handler={Projects}/>
        <Router.Route name="imagesList" path="/set/images/:id" handler={Project}/>
        <Router.Route name="imagesDetail" path="/set/images/:id/:num" handler={ProjectDetail}/>

        <Router.Route name="clientsList" path="/set/clients/:id" handler={Project}/>
        <Router.Route name="clientsDetail" path="/set/clients/:id/:num" handler={ProjectDetail}/>

        <Router.Route name="information" path="/information" handler={Information}/>
        <Router.DefaultRoute handler={Home}/>
    </Router.Route>
    /* jshint ignore:end */
);

let routeType = (location.hostname === 'localhost') ? Router.HashLocation : Router.HistoryLocation;

Router.run(routes, routeType, (Handler) => {
    let path = BasePath();
    let data = {};

    if (path === '' || path === '#/') path = 'home';
    data = {id: path};

    /* jshint ignore:start */
    React.render(<Handler data={data} />, document.body);
    /* jshint ignore:end */
});
