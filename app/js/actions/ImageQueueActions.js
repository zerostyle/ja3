let Reflux = require('reflux');

let ImageQueueActions = Reflux.createActions(['imageLoaded']);

export default ImageQueueActions;