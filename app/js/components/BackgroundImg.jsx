import React from 'react';
import TweenMax from '../lib/greensock/TweenMax.js';

let BackgroundImg = React.createClass({

    getDefaultProps() {
        return {
            time: 0.75,
            mulit: 0.1,
            ease: Quad.easeInOut
        };
    },

    componentDidMount() {
        this.bgContent = React.findDOMNode(this.refs.bgContent);
        TweenMax.set(this.bgContent, {autoAlpha: 0});
    },

    show() {
        TweenMax.to(this.bgContent, 0.5, {autoAlpha: 1, ease:Quad.easeOut});
    },

    transitionIn() {
        TweenMax.to(this.bgContent, 0.5, {autoAlpha: 1, ease:Quad.easeOut});
    },

    render() {
        return (
            <div ref="container" className="bg-img">
                <div ref="bgContent" className="bg-content"><div className="fs-bg-img" style={{backgroundImage: 'url(' + this.props.src + ')'}}></div></div>
            </div>
        );
    }

});

export default BackgroundImg;
