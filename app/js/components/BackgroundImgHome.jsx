import React from 'react';
import times from 'lodash/utility/times';
import classNames from 'classnames';
import TweenMax from '../lib/greensock/TweenMax.js';
import TimelineMax from '../lib/greensock/TimelineMax.js';
import HomeActions from '../actions/HomeActions.js';
import load from '../utils/imageLoader.js';

let BackgroundImgHome = React.createClass({

    getDefaultProps() {
        return {
            classes: 'fs-bg-img',
            color: '#2A2A2A',
            time: 0.75,
            mulit: 0.15,
            ease: Quad.easeInOut
        };
    },

    componentDidMount() {
        this.arr = [];
        times(9, (i) => {
            this.arr.push( React.findDOMNode(this.refs['r' + i]) );
        });

        this.container = React.findDOMNode(this.refs.container);
        TweenMax.set(this.container, {opacity: 0});

        this.tl = new TimelineMax();

        HomeActions.loading(this.props.id, 'image');

        load(this.props.src).then(() => {
            HomeActions.ready(this.props.id, 'image');
        });
    },

    componentWillUnmount() {
        this.tl.kill();
    },

    transitionIn() {
        TweenMax.set(this.container, {opacity: 1});
        TweenMax.set(this.arr, {opacity: 1, strokeOpacity: 1});

        let tweenObj = {opacity: 0, strokeOpacity: 0, ease: this.props.ease};

        this.tl.clear();
        this.tl.to(this.arr[4], this.props.time, tweenObj, 'start');
        this.tl.to([this.arr[3], this.arr[5]], this.props.time, tweenObj, `start+=${ this.props.mulit * 1 }`);
        this.tl.to([this.arr[2], this.arr[6]], this.props.time, tweenObj, `start+=${ this.props.mulit * 2 }`);
        this.tl.to([this.arr[1], this.arr[7]], this.props.time, tweenObj, `start+=${ this.props.mulit * 3 }`);
        this.tl.to([this.arr[0], this.arr[8]], this.props.time, tweenObj, `start+=${ this.props.mulit * 4 }`);
        this.tl.to(this.container, 5, {});
        this.tl.addCallback(this.transitionOut);
    },

    transitionOut() {
        let tweenObj = {opacity: 1, strokeOpacity: 1, ease: this.props.ease};

        this.tl.clear();
        this.tl.to([this.arr[0], this.arr[8]], this.props.time, tweenObj, 'end');
        this.tl.to([this.arr[1], this.arr[7]], this.props.time, tweenObj, `end+=${ this.props.mulit * 1 }`);
        this.tl.to([this.arr[2], this.arr[6]], this.props.time, tweenObj, `end+=${ this.props.mulit * 2 }`);
        this.tl.to([this.arr[3], this.arr[5]], this.props.time, tweenObj, `end+=${ this.props.mulit * 4 }`);
        this.tl.to(this.arr[4], this.props.time, tweenObj, `end+=${ this.props.mulit * 5 }`);
        this.tl.to(this.container, 0.5, {opacity: 0});
        this.tl.addCallback(this.animationComplete);
    },

    animationComplete(){
        HomeActions.animationComplete(this.props.id, 'image');
    },

    render() {
        let bgClass = classNames(this.props.classes, {
            'cover': this.props.cover !== ''
        });

        let offsetClass = classNames('bg-img', {
            'page-top-padding': this.props.cover === ''
        });

        return (
            <div ref="container" className={offsetClass}>
                <div className="bg-content">
                    <div className={bgClass} style={{backgroundImage: 'url(' + this.props.src + ')'}}></div>
                </div>
                <div className="bg-mask">
                    <svg viewBox="0 0 1024 720" preserveAspectRatio="none">
                        <g id="mask" fill={this.props.color} stroke={this.props.color} strokeWidth={1} >
                            <rect ref="r0" x={0} y={0} width={1024} height={80} />
                            <rect ref="r1" x={0} y={80} width={1024} height={80} />
                            <rect ref="r2" x={0} y={160} width={1024} height={80} />
                            <rect ref="r3" x={0} y={240} width={1024} height={80} />
                            <rect ref="r4" x={0} y={320} width={1024} height={80} />
                            <rect ref="r5" x={0} y={400} width={1024} height={80} />
                            <rect ref="r6" x={0} y={480} width={1024} height={80} />
                            <rect ref="r7" x={0} y={560} width={1024} height={80} />
                            <rect ref="r8" x={0} y={640} width={1024} height={80} />
                        </g>
                    </svg>
                </div>
            </div>
        );
    }

});

export default BackgroundImgHome;
