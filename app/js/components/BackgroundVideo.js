import React from 'react';
import TweenMax from '../lib/greensock/TweenMax.js';
import parseVideoURL from '../utils/parseVideoURL.js';
import { get } from 'lodash';

let BackgroundVideo = React.createClass({

    getInitialState() {
        return {
          video: null,
        };
      },

    getDefaultProps() {
        return {
            time: 0.75,
            mulit: 0.1,
            ease: Quad.easeInOut
        };
    },

    componentWillMount() {
        const isVideo = this.props.src.length > 3;

        if (isVideo) {
            const video = parseVideoURL(this.props.src);
            if (get(video, 'error')) {
                console.warn('Error', video);
            } else {
                this.setState({video})
            }
        }
    },

    componentDidMount() {
        this.bgContent = React.findDOMNode(this.refs.bgContent);
        TweenMax.set(this.bgContent, {autoAlpha: 0});
    },

    show() {
        TweenMax.to(this.bgContent, 0.5, {autoAlpha: 1, ease:Quad.easeOut});
    },

    transitionIn() {
        TweenMax.to(this.bgContent, 0.5, {autoAlpha: 1, ease:Quad.easeOut});
    },

    getVideoPlayer(video) {
        const { type, id } = video
        if (type === 'youtube') {
            return (
                <iframe
                    ref={i => this.iframe = i}
                    id="ytplayer"
                    type="text/html"
                    width="640"
                    height="360"
                    src={`https://www.youtube.com/embed/${id}`}
                    frameBorder="0"
                />
            )
        } else if (type === 'vimeo') {
            return (
                <iframe
                    ref={i => this.iframe = i}
                    src={`https://player.vimeo.com/video/${id}`}
                    width="640"
                    height="360"
                    frameBorder="0"
                    webkitAllowFullScreen
                    mozAllowFullScreen
                    allowFullScreen
                />
            )
        }

        return null
    },

    render() {
        const { video } = this.state
        return (
            <div ref="container" className="bg-img">
                <div ref="bgContent" className="bg-content bg-video">
                    {this.props.active && video && this.getVideoPlayer(video)}
                </div>
            </div>
        );
    }

});

export default BackgroundVideo;
