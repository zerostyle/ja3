let React = require('react');
let ReactSwipe = require('./Swipe.js');
let Actions = require('../actions/Actions.js');
let HotKey = require('react-hotkey');
let BackgroundImg = require('./BackgroundImg.jsx');
import BackgroundVideo from './BackgroundVideo'
let Router = require('react-router');
let ImageQueueStore = require('../stores/ImageQueueStore.js');

let Carousel = React.createClass({
    mixins: [HotKey.Mixin('handleHotkey'), Router.Navigation],

    directionIsForward: true,

    componentWillMount() {
        this.setState({num: this.props.num});
        this.setState({currentSlideNum: this.props.num});

        //console.log('componentWillMount', this.props, this.state);
    },

    componentDidMount() {
        HotKey.activate();

        let index = this.state.num -1,
            total = this.props.images.length,
            len = total - index,
            item, img;

        // > queue images from index
        this.loopImages(index, total);

        // < load images before index
        if (index > 0) this.loopImages(0, index);

        ImageQueueStore.processQueue();

        Actions.projectDetailShow();
    },

    componentWillUnmount() {
        ImageQueueStore.cancelAllLoads();
        Actions.projectDetailHide();
    },

    loopImages(index, len){
        let item;
        let img;

        for (let i = index; i < len; i++) {
            item = this.refs['img' + i];
            img = this.props.images[i].lrg;
            ImageQueueStore.addImage(img, this.onLoadComplete({item: item}));
        }
    },

    handleHotkey(e) {
        let LEFT_ARROW = 37;
        let RIGHT_ARROW = 39;
        let ESC = 27;

        switch (e.keyCode) {
            case LEFT_ARROW:
                this.changeDirection(false);
                this.prevImage();
                break;
            case RIGHT_ARROW:
                this.changeDirection(true);
                this.nextImage();
                break;
            case ESC:
                this.closeView();
                break;
        }
    },

    changeDirection(isForward) {
        // Change direction of load que, if user moves in oposite direction
        if (isForward !== this.directionIsForward) ImageQueueStore.reverseQueue();
        this.directionIsForward = isForward;
    },

    onLoadComplete(e){
        return (error, result) => {
            if (result){
                // console.log(e.item.props.src);
                e.item.setState({ready: true});
                e.item.transitionIn();
            } else {
               console.warn('Error in', e.item, error);
            }
        };
    },

    nextImage () {
        this.refs.ReactSwipe.swipe.next();
    },

    prevImage () {
        this.refs.ReactSwipe.swipe.prev();
    },

    onClickImage() {
        this.nextImage();
    },

    closeView(){
        let paths = this.props.path.split('/');
        this.transitionTo(paths[0] + 'List', {id:paths[1]});
    },

    goToHome() {
        this.transitionTo('home');
    },

    onChange(index, el) {
        let item = this.refs['img' + index],
            img = this.props.images[index].lrg,
            len = this.props.images.length -1;

        item.setState({ready: true});
        item.show();

        let nextNum = index + 1;
        if (nextNum > len) nextNum = 0;

        let previousNum = index - 1;
        if (previousNum < 0) previousNum =  len;

        // load next + prev
        if (this.directionIsForward) {
            this.refs['img' + nextNum].setState({ready: true});
            this.refs['img' + previousNum].setState({ready: true});
        } else {
            this.refs['img' + previousNum].setState({ready: true});
            this.refs['img' + nextNum].setState({ready: true});
        }

        // add to load queue
        //ImageQueueStore.addImage(img, this.onLoadComplete({msg:img, item:item}));
        //ImageQueueStore.processQueue();
        ImageQueueStore.prioritizeQueue(img);

        // Update route
        let paths = this.props.path.split('/');
        this.transitionTo(paths[0] + 'Detail', {id: paths[1], num: index+1});

        this.setState({currentSlideNum: index + 1});
    },

    onTransitionEnd(index, el){
        // console.log('onTransitionEnd', index);
    },

    render() {
        const getItem = (item, i) => {
            const isVideo = item.video.length > 3
            const Background = isVideo ? BackgroundVideo : BackgroundImg;

            return (
                <div key={i} className="fs-img">
                    <Background
                        ref={'img' + i}
                        src={isVideo ? item.video : item.lrg}
                        active={i === this.state.currentSlideNum - 1}
                    />
                </div>
            );
        }

        return (
            /* jshint ignore:start */
            <div className="fs fade-in2">
                <div className="fs-nav">

                    <div className="fs-nav-row fs-nav-high">

                        <div className="fs-btn fs-mark" onClick={this.goToHome}>
                            <svg className="ja-grey" x="0px" y="0px" viewBox="-289 385 32 23">
                               <polygon className="ja-grey-shape" points="-271,385 -271,385 -271,385 -279.9,400.2 -279.9,400.2 -288.9,400.2 -284.4,407.9 -275.5,407.9 -271,400.3 -266.5,407.9 -257.6,407.9 " />
                            </svg>
                        </div>

                        <div className="fs-btn fs-close " onClick={this.closeView}>
                            <svg className="close-svg" width="22px" height="22px" viewBox="0 0 22 22">
                                <g transform="translate(1.000000, 1.000000)">
                                    <path className="close-line" d="M0.347826087,19.5454545 L19.7391304,0.454545455" />
                                    <path className="close-line" d="M19.6521739,19.5454545 L0.260869565,0.454545455" />
                                </g>
                            </svg>
                        </div>
                    </div>

                    <div className="swipe-c" onClick={this.onClickImage} >

                        <ReactSwipe
                            ref="ReactSwipe"
                            startSlide={this.state.num - 1}
                            continuous={true}
                            callback={this.onChange}
                            transitionEnd={this.onTransitionEnd}
                            currentSlideNum={this.state.currentSlideNum}
                        >
                           {this.props.images.map(getItem)}
                        </ReactSwipe>

                    </div>

                    <div className="fs-nav-row fs-nav-low">
                        <div className="fs-btn prev-btn" onClick={this.prevImage}>
                            <svg width="23px" height="27px" viewBox="0 0 23 27" >
                                <g stroke="none" fill="none" fill-rule="evenodd" >
                                  <path d="M21,13.5 L1,2 L1,25 L21,13.5 L21,13.5 Z" className="arrow-path" />
                                </g>
                            </svg>
                        </div>

                        <div className="slide-count">{this.state.currentSlideNum} / {this.props.images.length}</div>

                        <div className="fs-btn next-btn" onClick={this.nextImage}>
                            <svg width="23px" height="27px" viewBox="0 0 23 27" >
                                <g stroke="none" fill="none" fill-rule="evenodd" >
                                  <path d="M21,13.5 L1,2 L1,25 L21,13.5 L21,13.5 Z" className="arrow-path" />
                                </g>
                            </svg>
                        </div>
                    </div>


                </div>

            </div>
            /* jshint ignore:end */
        );
    }

});

export default Carousel;
