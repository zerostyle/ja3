import React from 'react';
import Router from 'react-router';
import ImageComponent from '../components/ImageComponent.js';
import parseVideoURL from '../utils/parseVideoURL.js';
import classNames from 'classnames';
import { get } from 'lodash';

const ImageButton = React.createClass({

	getInitialState() {
    return {
      isVideo: false,
    };
  },

	componentWillMount() {
		const isVideo = this.props.data.video.length > 3;

		if (isVideo) {
			const videoObject = parseVideoURL(this.props.data.video);
			if (get(videoObject, 'error')) {
				console.warn('Error', videoObject);
			} else {
				this.setState({isVideo})
			}
		}
	},

	render() {
		const toLink = this.props.data.set + 'Detail';
		const cls = classNames('img-detail', {'img-detail-video': this.state.isVideo});

		return (
			/* jshint ignore:start */
			<div className={cls}>
				<Router.Link to={toLink} params={{id: this.props.data.setID, num: this.props.data.index + 1}}>
					<ImageComponent src={this.props.data.med} gridImageLoaded={this.props.data.gridImageLoaded} />
					{this.state.isVideo && (
						<svg width={60} height={50} viewBox="0 0 28 32">
					  	<path d="M28.66,16l-29,16V0Z"/>
						</svg>
					)}
				</Router.Link>
			</div>
			/* jshint ignore:end */
		);

	}

});

export default  ImageButton;