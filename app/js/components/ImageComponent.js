let React = require('react');
let classNames = require('classnames');

/** No JSX harmony support, polyfill Object.assign! **/
if (!Object.assign) {
  Object.defineProperty(Object, "assign", {
    enumerable: false,
    configurable: true,
    writable: true,
    value: (target, firstSource) => {
      "use strict";
      if (target === undefined || target === null) throw new TypeError("Cannot convert first argument to object");

      let to = Object(target);
      for (let i = 1; i < arguments.length; i++) {
        let nextSource = arguments[i];
        if (nextSource === undefined || nextSource === null) continue;
        let keysArray = Object.keys(Object(nextSource));
        for (let nextIndex = 0, len = keysArray.length; nextIndex < len; nextIndex++) {
          let nextKey = keysArray[nextIndex];
          let desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);
          if (desc !== undefined && desc.enumerable) to[nextKey] = nextSource[nextKey];
        }
      }
      return to;
    }
  });
}

let ImageComponent = React.createClass({
  getInitialState() {
    return {
      loaded: false
    };
  },

  onImageLoad() {
    if (this.isMounted()) {
      this.setState({loaded: true});
      if (this.props.gridImageLoaded) this.props.gridImageLoaded();
    }
  },

  componentDidMount() {
    let imgTag = this.refs.img.getDOMNode();
    let imgSrc = imgTag.getAttribute('src');
    let img = new window.Image();
    img.onload = this.onImageLoad;
    img.src = imgSrc;
  },

  render() {
    let classes = classNames('image', {
      'image-loaded': this.state.loaded
    });

    return React.createElement('img', Object.assign({}, this.props, {
      ref: 'img',
      className: classes
    }));
  }

});

export default ImageComponent;
