let React = require('react');
let Masonry = require('masonry-layout');
let MasonryMixin = require('react-masonry-mixin');
let ImageButton = require('../components/ImageButton.js');

var masonryOptions = { transitionDuration: 0 };

var ImageGrid = React.createClass({
    mixins: [MasonryMixin('masonryContainer', masonryOptions)],

    gridImageLoaded: function(){
        this.masonry.layout();
    },

    render: function() {
        
        let imgBtn = (item, i) => {
            item.index = i;
            item.set = this.props.data.set;
            item.setID = this.props.data.setID;
            item.gridImageLoaded = this.gridImageLoaded;

            return (
                /* jshint ignore:start */
                <ImageButton key={item.id} data={item} />
                /* jshint ignore:end */
            );
        }

        return (
            /* jshint ignore:start */
            <div className="grid" ref="masonryContainer">
                {this.props.data.images.map(imgBtn)}
            </div>
            /* jshint ignore:end */
        );

    }

});

export default  ImageGrid;