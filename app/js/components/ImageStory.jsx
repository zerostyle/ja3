import React from 'react';
import ImageButton from '../components/ImageButton.js';

let ImageStory = React.createClass({

    gridImageLoaded() {},

    render() {
        let imgBtn = (item, i) => {
            item.index = i;
            item.set = this.props.set;
            item.setID = this.props.setID;
            item.gridImageLoaded = this.gridImageLoaded;

            return (
                <ImageButton key={item.id} data={item} />
            );
        };

        return (
            <div className="story" ref="story">
                {this.props.images.map(imgBtn)}
            </div>
        );
    }

});

export default ImageStory;
