let React = require('react');
let ImageComponent = require('../components/ImageComponent.js');

var Img = React.createClass({

	render: function() {

		return (
			/* jshint ignore:start */
			<ImageComponent src={this.props.data.src} />
			/* jshint ignore:end */
		);

	}

});

export default Img;