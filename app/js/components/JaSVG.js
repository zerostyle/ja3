var React = require('react');

var JaSVG = React.createClass({

	render: function() {

		return (
			/* jshint ignore:start */
			<svg className="ja-svg" viewBox="0 0 32 23" xmlns="http://www.w3.org/2000/svg" version="1.1">
				<g stroke="none" fill="none" fill-rule="evenodd">
					<path className="ja-shape" d="M18 0L18 0 9.1 15.2 0.1 15.2 4.6 22.9 13.5 22.9 18 15.3 22.5 22.9 31.4 22.9 18 0Z" fill={this.props.fill} />
				</g>
		   	</svg>
			/* jshint ignore:end */
		);

	}

});

export default  JaSVG;