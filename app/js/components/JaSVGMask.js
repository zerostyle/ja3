var React = require('react');

var JaSVGMask = React.createClass({

  render: function() {

    return (
      /* jshint ignore:start */
      <svg className="ja-svg-mask" viewBox="0 0 502 375" version="1.1">
        <clipPath id="clipMask">
          <path d="M502.33 374.47L352.89 374.47 286.3 259.15 219.72 374.47 75.69 374.47 0.95 244.66 144.99 244.66 286.47 0 502.33 374.47 502.33 374.47ZM358.32 365.06L486.05 365.06 286.46 18.81 150.42 254.07 17.22 254.07 81.13 365.06 214.29 365.06 286.29 240.33 358.32 365.06 358.32 365.06Z" />
          <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
            <g transform="translate(-470.000000, -293.000000)">
              <g transform="translate(469.000000, 293.000000)">
                <mask fill="white" />
                <use fill="#231F1F" />
              </g>
            </g>
          </g>
        </clipPath>
      </svg>
      /* jshint ignore:end */
    );

  }

});

export default JaSVGMask;