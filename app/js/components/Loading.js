var React = require('react');

var Loading = React.createClass({

	render: function() {

		return (
			/* jshint ignore:start */
			<div className="tri-loader">
			    <div className="tri-c">
			    	<div className="triangles">
				    	<div className="tri invert" />
				    	<div className="tri invert" />
				    	<div className="tri" />
				    	<div className="tri invert" />
				    	<div className="tri invert" />
				    	<div className="tri" />
				    	<div className="tri invert" />
				    	<div className="tri" />
				    	<div className="tri invert" />
				    </div>
			    </div>
			</div>
			/* jshint ignore:end */
		);

	}	

});

export default  Loading;