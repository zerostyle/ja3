var React = require('react');

var LoadingImage = React.createClass({

	render: function() {

		return (
			/* jshint ignore:start */
			    <div className="tri-c">
			    	<div className="triangles">
				    	<div className="tri invert" />
				    	<div className="tri invert" />
				    	<div className="tri" />
				    	<div className="tri invert" />
				    	<div className="tri invert" />
				    	<div className="tri" />
				    	<div className="tri invert" />
				    	<div className="tri" />
				    	<div className="tri invert" />
				    </div>
			    </div>
			/* jshint ignore:end */
		);

	}	

});

export default LoadingImage;