import React from 'react';
import isNull from 'lodash/lang/isNull';
import shuffle from 'lodash/collection/shuffle';
import { ListenerMixin } from 'reflux';
import Pixi from'pixi.js';
import PixiAnimation from '../pixi/PixiAnimation.js';
import MaskAnimation from '../pixi/MaskAnimation.js';
import HomeStore from '../stores/HomeStore.js';

let PixiSlide = React.createClass({
    mixins: [ ListenerMixin ],
    frame: null,

    componentDidMount() {
        this.div = React.findDOMNode(this.refs.pixi);

        this.renderer = new PIXI.WebGLRenderer(0, 0, {transparent: true, resolution: 1});
        this.div.appendChild(this.renderer.view);

        this.container = new PIXI.Container();

        this.pixi0 = new PixiAnimation({container: this.container, div: this.div});
        this.pixi1 = new MaskAnimation({container: this.container, div: this.div, images: shuffle(this.props.images)});

        this.animate();

        this.handleResize();
        window.addEventListener('resize', this.handleResize);

        this.listenTo(HomeStore, this.nextImage);
    },

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleResize);
        if (!isNull(this.frame)) cancelAnimationFrame(this.frame);

        this.pixi0.componentWillUnmount();
        this.pixi1.componentWillUnmount();
    },

    nextImage(obj) {
        if (obj.type === 'pixi') {
            this['pixi' + obj.id].transitionIn();

            while (this.renderer.view.height < 1) {
                this.handleResize();
            }
        }
    },

    handleResize() {
        let viewWidth = window.innerWidth;
        let viewHeight = this.div.offsetHeight;

        this.renderer.resize(viewWidth, viewHeight, true);
    },

    animate() {
        this.frame = requestAnimationFrame(this.animate);
        this.renderer.render(this.container);
    },

    render() {

        return (
            /* jshint ignore:start */
            <div ref="pixi" className="pixi-slide" />
            /* jshint ignore:end */
        );
    }

});

export default PixiSlide;