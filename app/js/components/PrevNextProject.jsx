import React  from 'react';
import Router from 'react-router';
import classNames from 'classnames';
import load from '../utils/imageLoader.js';

let PrevNextProject = React.createClass({

    getInitialState() {
        return {
            ready: false
        };
    },

    componentWillMount() {
        this.loadImages(this.props);
    },

    componentWillReceiveProps(nextProps) {
        this.loadImages(nextProps);
    },

    loadImages(prevNext) {
        this.setState({ready: false});

        let prevImg = prevNext.prev.images[prevNext.prev.image].med;
        let nextImg = prevNext.next.images[prevNext.next.image].med;

        let loader = Promise.all([
             load(prevImg),
             load(nextImg)
         ]).then(
            this.onComplete
         );
    },

    onComplete(){
        this.setState({ready: true});
    },

    render() {
        let bgImg = classNames('skip-bg', {
          'hide': !this.state.ready
        });

        if (!this.state.ready) return <div className="prev-next" />;

        return (
            /* jshint ignore:start */
            <div className="prev-next">

                <div className="skip-project prev-project" >
                    <Router.Link to={this.props.path} params={{id: this.props.prev.id}}>
                        <div className="skip-c">
                            <div className={bgImg} style={{backgroundImage: "url(" + this.props.prev.images[this.props.prev.image].med + ")"}}></div>
                            <div className="skip-layer"><div>{this.props.prev.title}</div></div>
                        </div>
                    </Router.Link>
                </div>

                <div className="skip-project next-project" >
                    <Router.Link to={this.props.path} params={{id: this.props.next.id}}>
                        <div className="skip-c">
                            <div className={bgImg} style={{backgroundImage: "url(" + this.props.next.images[this.props.next.image].med + ")"}}></div>
                            <div className="skip-layer"><div>{this.props.next.title}</div></div>
                        </div>
                    </Router.Link>
                </div>
            </div>
            /* jshint ignore:end */
        );

    }
});

export default PrevNextProject;