import React from 'react';
import classNames from 'classnames';
import load from '../utils/imageLoader.js';
import TweenMax from '../lib/greensock/TweenMax.js';
import TimelineMax from '../lib/greensock/TimelineMax.js';
import SplitText from '../lib/greensock/utils/SplitText.js';

let ProjectHero = React.createClass({

	getInitialState() {
		return {
			ready: false
		};
  	},

	itemLoaded(){
		this.setState({ready:true});
	},

	componentWillMount() {
		this.setState({ready:false});

		load( this.props.images[this.props.image].lrg ).then(
        	this.itemLoaded
        );
	},

	componentDidUpdate(prevProps, prevState) {
		this.animateText();
		document.body.scrollTop = 0;
	},

	componentDidMount(){
		this.animateText();
	},

	animateText() {
		let txt = React.findDOMNode(this.refs.heroTitle);
		let el = React.findDOMNode(this.refs.heroTitle);

		let tl = new TimelineMax({delay:1.5});
		let mySplitText = new SplitText(el, {type:"words,chars"});
		let chars = mySplitText.chars;

		TweenMax.set(el, {perspective:400});
		tl.staggerFrom(chars, 0.75, {opacity:0, ease:Quad.easeInOut}, 0.02, "start");
		tl.staggerFrom(chars, 0.75, {scaleX:0, y:80, rotationX:180, transformOrigin:"100% 0% 0%",  ease:Expo.easeOut}, 0.02, "start");
	},

	render() {
		let style = (this.state.ready) ? {backgroundImage: 'url(' + this.props.images[this.props.image].lrg + ')'} : {}; 

		let bgImg = classNames('hero-bg-c', {
			'img-loaded': this.state.ready
		});

		return (
			/* jshint ignore:start */
			<div className="project-hero" >
				<div className={bgImg}>
					<div className="hero-bg-img" style={style}></div>
				</div>
				<div className="hero-bg">
					<div ref="heroTitle" className="hero-title">
						{this.props.title}
					</div>
				</div>
			</div>
		    /* jshint ignore:end */
		);

	}

});

export default ProjectHero;