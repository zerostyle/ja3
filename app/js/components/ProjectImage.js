import React from 'react';
import Router from 'react-router';
import load from '../utils/imageLoader.js';
import classNames from 'classnames';


var ProjectImage = React.createClass({

	getInitialState : function() {
		return {
			loaded: false
		};
  	},

	componentWillMount() {
		load(this.props.image.lrg).then(
        	this.itemLoaded
        );
	},

	itemLoaded() {
		this.setState({loaded: true});
	},

	render() {
		let pContainer = classNames({ 
			'p-container': !this.state.loaded,
	      	'p-container-loaded': this.state.loaded
	    });

		let bgImg = classNames('image p-bg-img', { 
	      'p-bg-img-loaded': this.state.loaded
	    });

	    let css = {backgroundImage: "url(" + this.props.image.lrg + ")"};

		return	(
			/* jshint ignore:start */
			<div className="project-item">
				<Router.Link to={this.props.link} params={{id: this.props.id}}>
				<div className={pContainer}>
					<div ref="bgImg" className={bgImg} style={css} />
					<div className="p-label-container fade-in2">
						<div className="p-label">
							<div>{this.props.path}</div>
							<div className="sm-rule">
								<div className="sm-inside" />
							</div>
							<div className="p-title">{this.props.title}</div>
						</div>
					</div>
				</div>
				</Router.Link>
			</div>	
			/* jshint ignore:end */
		);
	}
});

export default ProjectImage;