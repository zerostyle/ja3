import React from 'react';
import keys from 'lodash/object/keys';
import ProjectImage from '../components/ProjectImage.js';
import AnimateOnMountMixin from '../mixins/AnimateOnMountMixin.js';

let ProjectsGrid = React.createClass({
    mixins: [ AnimateOnMountMixin ],

    render() {
        let items = keys(this.props.sets);

        let getItem = item => {
            let i = this.props.sets[item];
            let imgNum = Number(i.image);
            let props = {id: i.id, link: i.path + 'List', image: i.images[imgNum], path: i.path, title: i.title};

            return <ProjectImage key={i.id} {...props} />;
        };

        return (
            /* jshint ignore:start */
            <div ref="grid" className={this.props.classList}>
                {items.map(getItem)}
            </div>
            /* jshint ignore:end */
        );

    }
});

export default ProjectsGrid;
