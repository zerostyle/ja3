import React from 'react';
import { ListenerMixin } from 'reflux';
import BackgroundImgHome from '../components/BackgroundImgHome.jsx';
import HomeStore from '../stores/HomeStore.js';

let SlideShowImages = React.createClass({
    mixins: [ ListenerMixin ],

    getInitialState() {
        return {
            active: false
        };
    },

    componentDidMount() {
        this.listenTo(HomeStore, this.nextImage);
    },

    nextImage(obj) {
        if (obj.type !== 'pixi') {
            let item = this.refs['home' + obj.id];
            item.transitionIn();
        }
    },

    getImageAndLoad(item, i) {
        return <BackgroundImgHome key={'home' + i} ref={'home' + i} classes={'fs-bg-img'} src={item.lrg} color={'#E7E7E7'} id={i} cover={item.cover} />;
    },

    render() {

        return (
            <div className="'slide-item'">
                {this.props.images.map(this.getImageAndLoad)}
            </div>
        );
    }
});

export default SlideShowImages;
