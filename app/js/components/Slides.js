import React from 'react';
import { ListenerMixin } from 'reflux';
import classNames from 'classnames';
import PixiSlide from '../components/PixiSlide.js';
import SlideShowImages from '../components/SlideShowImages.jsx';
import HomeStore from '../stores/HomeStore.js';

let Slides = React.createClass({
    mixins: [ ListenerMixin ],

    getInitialState() {
        return {
            active: null
        };
    },

    componentWillMount() {
        if (!this.props.images.length) return;

        this.listenTo(HomeStore, this.nextImage);
    },

    nextImage(obj) {
        this.setState({active: obj.type});
    },

    render() {
        let imgClass = classNames('slide-item', {
            'hide': this.state.active !== 'image'
        });

        let pixiClass = classNames('slide-item', {
            'hide': this.state.active !== 'pixi'
        });

        return (
            /* jshint ignore:start */
            <div className="slides">
                <div className={imgClass}>
                    { this.props.fsImages.length > 0 ? <SlideShowImages images={this.props.fsImages} len={this.props.fsImages.length} /> : false }
                </div>
                <div className={pixiClass}>
                    <PixiSlide images={this.props.images} />
                </div>
            </div>
            /* jshint ignore:end */
        );
    }
});

export default Slides;

