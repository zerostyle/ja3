import React from 'react';
import isNull from 'lodash/lang/isNull';
import TweenMax from '../lib/greensock/TweenMax.js';
import TimelineMax from '../lib/greensock/TimelineMax.js';

let AnimateOnMountMixin = {

    componentDidMount() {
        this.animate();
    },

    componentDidUpdate() {
        this.animate();
    },

    animate() {
        let grid = React.findDOMNode(this.refs.grid);

        if (isNull(grid)) return;

        TweenMax.set(grid, {alpha: 0, y: '+=100'});

        let tl = new TimelineMax({delay: 0});
        tl.to(grid, 1.25, {alpha: 1, ease: Quad.easeOut}, 'start');
        tl.to(grid, 1, {y: '0', ease: Expo.easeOut}, 'start');
    }
};

export default AnimateOnMountMixin;
