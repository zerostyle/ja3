var KeyboardMixin = {
   
    componentDidMount: function() {
        document.body.addEventListener('onKeyDown', this.onKeyDown);
    },

    componentWillUnMount: function() {
        document.body.addEventListener('onKeyDown', this.onKeyDown);
    }
};

/*
handleKeyDown: function(e) {

    var ENTER = 13,
        LEFT_ARROW = 37,
        RIGHT_ARROW = 39;

    switch(e.keyCode) {
        case LEFT_ARROW: break;
        case RIGHT_ARROW: break;
    }

}

*/