import React from 'react';
import extend from 'lodash/object/extend';
import Store from '../stores/Store.js';

let LoadEventMixin = {

    contextTypes: {
        router: React.PropTypes.func
    },

    getInitialState() {
      return { loaded: false };
    },

    isLoaded() {
        let id = this.getSetPath();
        return id === 'clients' ? Store.data.loaded && Store.data.clientsLoaded : Store.data.loaded;
    },

    componentDidMount() {
        if (this.isLoaded())
            this.setState( extend( {loaded: true}, this.getDataFromSets(this.props.data.id) ) );
        else
            this.listenTo(Store, this.loadComplete);
    },

    componentWillReceiveProps(nextProps) {
        let previousID = this.props.data.id;
        let newID = nextProps.data.id;

        if (previousID !== newID) this.setState( extend( {loaded: true}, this.getDataFromSets(newID) ) );
    },

    componentWillUnmount() {
        this.setState({sets: []});
    },

    getSetPath() {
        let id = this.props.data.id;
        let paths = id.split('/');
        return paths[0];
    },

    getDataFromSets(newID) {
        let id = newID ? newID : this.props.data.id;
        let paths = id.split('/');
        let setID = paths[1] === 'story' ? paths[2] : paths[1];
        let setList = Store.data.pages[paths[0]];

        // home
        if (newID === 'home') return extend(setList, Store.homeSets);

        // Get list of sets, or set by id
        return paths.length === 1 ? setList : setList.sets[setID];
    },

    loadComplete() {
        if (this.isLoaded() && !this.state.loaded) this.setState( extend( {loaded: true}, this.getDataFromSets(this.props.data.id) ) );
    }
};

export default LoadEventMixin;
