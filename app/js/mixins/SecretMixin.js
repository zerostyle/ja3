import isNull from 'lodash/lang/isNull';
import React from 'react';
import ReactPassword from '../components/ReactPassword.js';

let SecretMixin = {

    componentWillMount() {
        this.getSecret();
    },

    getSecret() {
        let localSecret = localStorage.getItem(this.props.params.id);
        let showPassword = isNull(localSecret);
        this.setState({ showPassword });
    },

    onPasswordChange() {
        this.checkSecret();
    },

    onEnter() {
        this.checkSecret(false);
    },

    checkSecret(typing = true) {
        let secret = typing ? this.state.secret.substr(0, this.state.secret.length - 1) : this.state.secret;
        if (this.refs.password.state.value === secret) {
            localStorage.setItem(this.state.id, true);
            this.setState({ showPassword: false });
        }
    },

    secretInput() {
        return (
            /* jshint ignore:start */
            <div className="page secret page-top-margin">
                <div className="secret-container">
                    <ReactPassword ref="password"
                        revealed={ this.state.checked }
                        maxLength="25"
                        onChange={ this.onPasswordChange }
                        onEnter={this.onEnter}
                        id="secret-password"
                    />
                </div>
            </div>
            /* jshint ignore:end */
        );
    }

};

export default SecretMixin;
