import React from 'react';
import times from 'lodash/utility/times';
import TweenMax from '../lib/greensock/TweenMax.js';

let TransitionMixin = {

    getDefaultProps() {
        return {
            color: '#E7E7E7',
            time: 0.75,
            mulit: 0.15,
            ease: Quad.easeInOut
        };
    },

    componentDidUpdate(prevProps, prevState) {
        if (!prevState.loaded && this.state.loaded) {
            this.arr = [];
            times(9, (i) => {
                this.arr.push( React.findDOMNode(this.refs['r' + i]) );
            });
        }

        this.transitionIn();
    },

    transitionIn() {
        if (!this.state.loaded) return;

        let mask = React.findDOMNode(this.refs.mask);

        TweenMax.killTweensOf(this.arr);
        TweenMax.killTweensOf(mask);
        TweenMax.set(mask, {display: 'block'});
        TweenMax.set(this.arr, {opacity: 1, strokeOpacity: 1, strokeWidth: 1});
        TweenMax.staggerTo(this.arr, 0.25, {opacity: 0, strokeOpacity: 0, strokeWidth: 0, ease: this.props.ease, delay:0.5, onComplete: () => {
            TweenMax.set(mask, {display: 'none'});
        }}, 0.05);
    },

    getMask() {
        return (
            <div ref="mask" className="page-mask">
                <svg viewBox="0 0 1024 720" preserveAspectRatio="none">
                    <g id="mask" fill={this.props.color} stroke={this.props.color} strokeWidth={0} >
                        <rect ref="r0" x={0} y={0} width={1024} height={80} />
                        <rect ref="r1" x={0} y={80} width={1024} height={80} />
                        <rect ref="r2" x={0} y={160} width={1024} height={80} />
                        <rect ref="r3" x={0} y={240} width={1024} height={80} />
                        <rect ref="r4" x={0} y={320} width={1024} height={80} />
                        <rect ref="r5" x={0} y={400} width={1024} height={80} />
                        <rect ref="r6" x={0} y={480} width={1024} height={80} />
                        <rect ref="r7" x={0} y={560} width={1024} height={80} />
                        <rect ref="r8" x={0} y={640} width={1024} height={80} />
                    </g>
                </svg>
            </div>
        );
    }
};

module.exports = TransitionMixin;
