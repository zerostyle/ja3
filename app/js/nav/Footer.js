let React = require('react');
let Reflux = require('reflux');
let Actions = require('../actions/Actions.js');
import Store from '../stores/Store.js';
import siteURL from '../utils/siteURL.js';

let Footer = React.createClass({
    mixins: [Reflux.ListenerMixin],

    getInitialState() {
        return {
            hide: false,
            loaded: Store.data.loaded
        };
    },

    componentDidMount() {
        this.listenTo(Actions.projectDetailShow, this.onProjectDetailShow);
        this.listenTo(Actions.projectDetailHide, this.onProjectDetailHide);

        TweenMax.set(React.findDOMNode(this.refs.footer), {alpha: 0});

        if (this.state.loaded)
            this.fadeIn();
        else
            this.listenTo(Store, this.loadComplete);
    },

    componentDidUpdate(prevProps, prevState) {
        if (!prevState.loaded && this.state.loaded) this.fadeIn();
    },

    loadComplete() {
        this.setState({loaded: true});
    },

    fadeIn() {
        TweenMax.to(React.findDOMNode(this.refs.footer), 0.5, {alpha: 1, ease: Quad.easeOut});
    },

    onProjectDetailShow() {
        this.setState({hide: true});
    },

    onProjectDetailHide() {
        this.setState({hide: false});
    },

    render() {
        let footerClass = this.state.hide || !this.state.loaded ? 'footer hide' : 'footer';

        return (
            /* jshint ignore:start */
            <div ref="footer" className={footerClass}>
                <div className="footer-contact">
                    <div className="ja-small"><img src={siteURL + '/assets/svg/ja-small.svg'} /></div>
                    <div className="v-rule" />
                    <div className="contact-link"><a href="mailto:info@jatecson.com" target="_blank">info@jatecson.com</a></div>
                </div>
                <div className="footer-social">
                    <div className="social-item"><div className="img-link"><a href="https://www.facebook.com/ja.tecson" target="_blank"><img src={siteURL + '/assets/svg/facebook.svg'} /></a></div></div>
                    <div className="social-item"><div className="img-link"><a href="https://instagram.com/jatecson" target="_blank"><img src={siteURL + '/assets/svg/instagram.svg'} /></a></div></div>
                    <div className="social-item"><div className="img-link"><a href="https://twitter.com/jatecson" target="_blank"><img src={siteURL + '/assets/svg/twitter.svg'} /></a></div></div>
                    <div className="social-item"><div className="img-link"><a href="http://blog.jatecson.com" target="_blank"><img src={siteURL + '/assets/svg/tumblr.svg'} /></a></div></div>
                    <div className="social-item"><div className="img-link"><a href="https://vimeo.com/user13335604" target="_blank"><img src={siteURL + '/assets/svg/vimeo.svg'} /></a></div></div>
                </div>
            </div>
            /* jshint ignore:end */
        );
    }
});

export default Footer;
