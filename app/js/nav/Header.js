let React = require('react');
let Reflux = require('reflux');
let Router = require('react-router');
let ExecutionEnvironment = require('react/lib/ExecutionEnvironment');
let Actions = require('../actions/Actions.js');
let PageNav = require('./PageNav.js');
import siteURL from '../utils/siteURL.js';

let Header = React.createClass({
    mixins: [ Router.State, Reflux.ListenerMixin],

    getInitialState() {
        return {
            scrollTop: 0,
            opacity: 1,
            bgOpacity: 0.25,
            showDarkNav: true,
            menuOpacity: 0,
            showJaButton: true,
            hide: false
        };
    },

    componentDidMount() {
        if (ExecutionEnvironment.canUseDOM) window.addEventListener('scroll', this.onScroll);

        this.listenTo(Actions.closeSideMenu, this.onCloseMenu);
        this.listenTo(Actions.projectDetailShow, this.onProjectDetailShow);
        this.listenTo(Actions.projectDetailHide, this.onProjectDetailHide);
    },

    componentWillUnmount() {
        window.removeEventListener('scroll', this.onScroll);
    },

    onProjectDetailShow() {
        this.setState({hide: true});
    },

    onProjectDetailHide() {
        this.setState({hide: false});
    },

    onCloseMenu() {
        this.setState({showJaButton: true});
    },

    onMenuClick() {
        Actions.openSideMenu();
        this.setState({showJaButton: false});
    },

    onScroll() {
        let scrollTop = document.body.scrollTop;

        let opacity = 1 - (scrollTop / 80);
        if (opacity < 0) opacity = 0;

        let bgOpacity = 0.25 - (scrollTop / 80);
        if (bgOpacity < 0) bgOpacity = 0;

        // Mobile menu button
        let menuOpacity = scrollTop - 60;
        if (menuOpacity < 0) menuOpacity = 0;
        if (menuOpacity > 100) menuOpacity = 100;

        this.setState({menuAlpha: scrollTop, opacity: opacity, bgOpacity: bgOpacity, menuOpacity: menuOpacity / 100});
    },

    render() {
        let headerStyle = {backgroundColor: `rgba(0, 0, 0, ${this.state.bgOpacity})`};

        let jaBtnStyle = this.state.showJaButton ? {opacity: 1} : {opacity: 0};

        let headerClass = this.state.showDarkNav ? 'ja-header' : 'ja-header-home';
        if (this.state.hide) headerClass += ' hide';

        let menuBtnStyle = {opacity: this.state.menuOpacity};

        let menuClass = this.state.menuOpacity === 0 ? 'side-nav-btn hide' : 'side-nav-btn';

        return (
            /* jshint ignore:start */
            <div className={headerClass} style={headerStyle}>
                <div className="nav-container">
                    <Router.Link to="home" className="home-btn">
                        <div className="mark" style={jaBtnStyle}>
                            <img src={siteURL + '/assets/svg/ja3.svg'} />
                        </div>
                    </Router.Link>
                    <PageNav opacity={this.state.opacity} />
                    <div className={menuClass} style={menuBtnStyle} onClick={this.onMenuClick}>
                        <img src={siteURL + '/assets/svg/menu.svg'} />
                    </div>
                </div>
            </div>
            /* jshint ignore:end */
        );
    }
});

export default Header;
