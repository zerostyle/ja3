var React = require('react');
var Router = require('react-router');

var PageNav = React.createClass({

	render: function() {
		var style = {opacity: this.props.opacity};
		
		return (
			/* jshint ignore:start */
			<div className="nav" style={style}>
				<div className="nav-item" ><Router.Link to="projects">Projects</Router.Link></div>
				<div className="nav-item" ><Router.Link to="images">Images</Router.Link></div>
				<div className="spacer" />
				<div className="nav-item" ><Router.Link to="information">Info<span className="hide-450">rmation</span></Router.Link></div>
			</div>
			/* jshint ignore:end */
		);
	}
});

export default PageNav;