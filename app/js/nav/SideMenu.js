import React from 'react/addons';
import Router from 'react-router';
import Reflux from 'reflux';
import delay from 'lodash/function/delay';
import Actions from '../actions/Actions.js';
import Tappable from 'react-tappable';
import TweenMax from '../lib/greensock/TweenMax.js';
import TimelineMax from '../lib/greensock/TimelineMax.js';
//import '../lib/greensock/plugins/ThrowPropsPlugin.js';
import siteURL from '../utils/siteURL.js';

var SideMenu = React.createClass({
	mixins: [Reflux.ListenerMixin, Router.Navigation],

	getInitialState: function() {
		return {
			menuX: window.innerWidth,
			showMenu: false
		};
  	},

  	componentDidMount: function() {
		this.listenTo(Actions.openSideMenu, this.onMenuOpen);
		window.addEventListener('resize', this.handleResize);

		this.getMenuWidth();

		var sideMenu = React.findDOMNode(this.refs.sideMenu),
			sideMenuContainer = React.findDOMNode(this.refs.sideMenuContainer);

		TweenMax.set(sideMenuContainer, {autoAlpha:0});
		this.closeMenuClick();
	},

	componentWillUnmount: function() {
		window.removeEventListener('resize', this.handleResize);
	},

	getMenuWidth: function(){
		var w = window.innerWidth;
		var menuX = (w < 500) ? 90 : w * 0.5;
		this.setState({menuX: menuX});
	},

	handleResize: function(){
		this.getMenuWidth();

		if (this.state.showMenu) {
			var sideMenu = React.findDOMNode(this.refs.sideMenu);
			TweenMax.set(sideMenu, {x: this.state.menuX});
		}
	},

	onMenuOpen: function(){
		this.setState({showMenu:true});

		var sideMenu = React.findDOMNode(this.refs.sideMenu),
			sideMenuContainer = React.findDOMNode(this.refs.sideMenuContainer);

		var tl = new TimelineMax();
		tl.to(sideMenu, 0.5, {x: this.state.menuX, ease:Expo.easeOut}, 'in');
		tl.to(sideMenuContainer, 0.2, {autoAlpha:1, ease:Quad.easeOut}, 'in');
		tl.play();

		document.body.style.overflow = 'hidden';
	},

	closeMenuClick: function(){
		Actions.closeSideMenu();
		this.setState({showMenu:false});

		var sideMenu = React.findDOMNode(this.refs.sideMenu),
			sideMenuContainer = React.findDOMNode(this.refs.sideMenuContainer);

		var tl = new TimelineMax();
		tl.to(sideMenu, 0.5, {x:window.innerWidth, ease:Expo.easeInOut}, 'out');
		tl.to(sideMenuContainer, 0.5, {autoAlpha:0, ease:Quad.easeInOut}, 'out');
		tl.play();

		document.body.style.overflow = 'auto';
	},

	closeMenuAndTransiton: function(link){
		this.closeMenuClick();
		delay(this.transitionTo, 200, link);
	},

	onClickGoToProjects: function(){
		this.closeMenuAndTransiton('projects');
	},

	onClickGoToImages: function(){
		this.closeMenuAndTransiton('images');
	},

	onClickGoToInfo: function(){
		this.closeMenuAndTransiton('information');
	},

	render: function() {
		var closeBtnStyle = this.state.showMenu ? {opacity: 1} : {opacity:0};

		return (
			/* jshint ignore:start */

			<div key="side-menu" className="side-menu" ref="sideMenuContainer">
				<div className="close-bg-btn" onClick={this.closeMenuClick} />
				<div className="close-nav-btn" onClick={this.closeMenuClick} style={closeBtnStyle}>
					<img src={siteURL + '/assets/svg/close.svg'} />
				</div>
				<div ref="sideMenu" className="sm-content">
					<div className="side-top">
						<Tappable onTap={this.onClickGoToProjects}><div className="nav-item"><div className="txt-link">Projects</div></div></Tappable>
						<Tappable onTap={this.onClickGoToImages}><div className="nav-item"><div className="txt-link">Images</div></div></Tappable>
						<div className="blue-rule" />
						<Tappable onTap={this.onClickGoToInfo}><div className="nav-item-small"><div className="txt-link">Information</div></div></Tappable>
					</div>
					<div className="side-bottom">
						<div className="social-item"><div className="img-link"><a href="https://www.facebook.com/ja.tecson" target="_blank"><img src={siteURL + '/assets/svg/facebook.svg'} /></a></div></div>
                    	<div className="social-item"><div className="img-link"><a href="https://instagram.com/jatecson" target="_blank"><img src={siteURL + '/assets/svg/instagram.svg'} /></a></div></div>
                    	<div className="social-item"><div className="img-link"><a href="https://twitter.com/jatecson" target="_blank"><img src={siteURL + '/assets/svg/twitter.svg'} /></a></div></div>
                    	<div className="social-item hide-400"><div className="img-link"><a href="http://blog.jatecson.com" target="_blank"><img src={siteURL + '/assets/svg/tumblr.svg'} /></a></div></div>
                    	<div className="social-item"><div className="img-link"><a href="https://vimeo.com/user13335604" target="_blank"><img src={siteURL + '/assets/svg/vimeo.svg'} /></a></div></div>
					</div>
				</div>
			</div>

			/* jshint ignore:end */
		);
	}
});

export default SideMenu;