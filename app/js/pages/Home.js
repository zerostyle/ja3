import Reflux from 'reflux';
import React from 'react';
import LoadEventMixin from '../mixins/LoadEventMixin.js';
import ProjectsGrid from '../components/ProjectsGrid.js';
import Slides from '../components/Slides.js';
import Loading from '../components/Loading.js';
import HomeActions from '../actions/HomeActions.js';
import siteURL from '../utils/siteURL.js';

let Home = React.createClass({
    mixins: [LoadEventMixin, Reflux.ListenerMixin],

    componentWillMount() {
        HomeActions.on();
    },

    componentWillUnmount() {
        HomeActions.off();
    },

    render() {

        if (this.state && this.state.loaded) {

            let css = {backgroundImage: 'url(' + siteURL + 'assets/img/p1.jpg)'};

            return (
                /* jshint ignore:start */
                <div className="home">
                    <Slides images={this.state.images} fsImages={this.state.fs_images} />
                    <div className="projects-images">
                        <ProjectsGrid sets={this.state.sets} classList="projects"/>
                    </div>
                </div>
                /* jshint ignore:end */
            );

        } else {
            return <Loading />;
        }
    }
});

module.exports = Home;
