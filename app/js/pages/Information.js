import Reflux from 'reflux';
import React from 'react';
import isNull from 'lodash/lang/isNull';
import LoadEventMixin from '../mixins/LoadEventMixin.js';
import JaSVG from '../components/JaSVG.js';
import Loading from '../components/Loading.js';
import TweenMax from '../lib/greensock/TweenMax.js';
import TimelineMax from '../lib/greensock/TimelineMax.js';
import siteURL from '../utils/siteURL.js';

let Information = React.createClass({
    mixins: [LoadEventMixin, Reflux.ListenerMixin],

    componentDidMount(){
        this.animate();
        window.addEventListener('resize', this.onResize);
    },

    componentWillUnmount() {
        window.removeEventListener('resize', this.onResize);
    },

    componentDidUpdate(prevProps, prevState) {
        this.animate();
    },

    animate() {
        let infoBg = React.findDOMNode(this.refs.infoBg);
        let txt = React.findDOMNode(this.refs.txt);

        if (isNull(infoBg)) return;

        let xDes = (document.body.clientWidth > 900) ? '-50%' : '50%';

        let userAgent = window.navigator.userAgent;
        let isSafari = userAgent.match(/iPad/i) || userAgent.match(/iPhone/i);

        let tl = new TimelineMax({onComplete: this.onResize, delay: 0.5});
        tl.from(infoBg, 0.75, {alpha: 0, ease:Quad.easeOut}, "start");
        tl.from(infoBg, 0.75, {x: xDes, ease: Expo.easeOut}, "start");
        tl.from(txt, 0.75, {x: '-50%', ease: Expo.easeOut}, "start");
        tl.from(txt, 0.75, {alpha: 0, ease: Quad.easeOut}, "start");
    },

    onResize(){
        let infoBg = React.findDOMNode(this.refs.infoBg);
        let txt = React.findDOMNode(this.refs.txt);
        TweenMax.set([infoBg, txt], {clearProps: "transform"});
        window.removeEventListener('resize', this.onResize);
    },

    render() {

        if (this.state && this.state.loaded) {

            let jaEmail = 'mailto:' + this.state.contact.ja.email;
            let jaPhone = 'tel:' + this.state.contact.ja.phone;
            let repEmail = 'mailto:' + this.state.contact.rep.email;
            let repPhone = 'tel:' + this.state.contact.rep.phone;

            let getImage = (item, i) => {
                return <div key={'info' + i} className="about-bg-img" style={{backgroundImage: "url(" + item.lrg + ")"}} />;
            };

            return (
                /* jshint ignore:start */
                <div className="page information">

                    <div className="contact-bg" />
                    <div ref="infoBg" className="contact-bg-img" />

                    <div className="contact-container">
                        <div className="contact">

                            <div ref="txt" className="txt">
                                <div ref="ja" className="c-item c-ja">
                                    <div className="c-label">{this.state.contact.ja.title}</div>
                                    <div className="c-title">{this.state.contact.ja.url.title}</div>
                                    <div className="txt-link"><a className="c-lo" href={jaEmail}>{this.state.contact.ja.email}</a></div>
                                    <div className="txt-link"><a href={jaPhone}>{this.state.contact.ja.phone}</a></div>
                                    <div className="social-container">
                                        <div className="social-item"><div className="img-link"><a href="https://www.facebook.com/ja.tecson" target="_blank"><img src={siteURL + '/assets/svg/facebook.svg'} /></a></div></div>
                                        <div className="social-item"><div className="img-link"><a href="https://instagram.com/jatecson" target="_blank"><img src={siteURL + '/assets/svg/instagram.svg'} /></a></div></div>
                                        <div className="social-item"><div className="img-link"><a href="https://twitter.com/jatecson" target="_blank"><img src={siteURL + '/assets/svg/twitter.svg'} /></a></div></div>
                                        <div className="social-item"><div className="img-link"><a href="http://blog.jatecson.com" target="_blank"><img src={siteURL + '/assets/svg/tumblr.svg'} /></a></div></div>
                                        <div className="social-item"><div className="img-link"><a href="https://vimeo.com/user13335604" target="_blank"><img src={siteURL + '/assets/svg/vimeo.svg'} /></a></div></div>
                                    </div>
                                </div>
                                <div ref="rep" className="c-item c-rep">
                                    <div className="c-label">{this.state.contact.rep.title}</div>
                                    <div className="c-title txt-link"><a href={this.state.contact.rep.url.value} target="_blank">{this.state.contact.rep.url.title}</a> </div>
                                    <div className="txt-link"><a className="c-lo" href={repEmail}>{this.state.contact.rep.email}</a></div>
                                    <div className="txt-link"><a href={repPhone}>{this.state.contact.rep.phone}</a></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="bio">
                        <p>{this.state.bio}</p>
                    </div>

                    <div className="contact-images">
                        <div className="bg-strip" />
                        <div className="c-image-container">
                            <div className="info-img-flex">
                                {this.state.contact.images.map(getImage)}
                            </div>
                        </div>
                    </div>

                    <div className="contact-message">
                        <div>{this.state.message}</div>
                    </div>

                    <div className="contact-low">
                        <div className="ja-red">
                            <JaSVG fill="#7D0102" />
                        </div>
                    </div>
                </div>
                /* jshint ignore:end */
            );

        } else {
            return <Loading />;
        }
    }
});

module.exports = Information;