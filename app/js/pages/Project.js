import Reflux from 'reflux';
import React from 'react';
import last from 'lodash/array/last';
import classNames from 'classnames';
import Store from '../stores/Store.js';
import LoadEventMixin from '../mixins/LoadEventMixin.js';
import ImageGrid from '../components/ImageGrid.js';
import Loading from '../components/Loading.js';
import SecretMixin from '../mixins/SecretMixin.js';
import TransitionMixin from '../mixins/TransitionMixin.jsx';
import AnimateOnMountMixin from '../mixins/AnimateOnMountMixin.js';
import PrevNextProject from '../components/PrevNextProject.jsx';
import ProjectHero from '../components/ProjectHero.jsx';

let Project = React.createClass({
    mixins: [LoadEventMixin, Reflux.ListenerMixin, SecretMixin, AnimateOnMountMixin, TransitionMixin],

    getInitialState() {
        return {
            checked: true
        };
    },

    render() {

        if (this.state && this.state.loaded) {
            let paths = this.props.data.id.split('/');
            let grid = {
                set: paths[0],
                setID: last(paths),
                images: this.state.images
            };

            let isClient = grid.set === 'clients';
            let showNextPrev = <span />;

            if (this.state.showPassword && this.state.secret) {
                return this.secretInput();
            } else {
                if (!isClient) {
                    let prevNext = Store.getPrevNextProject( grid.set, grid.setID );
                    showNextPrev = <PrevNextProject {...prevNext} />;
                }

                let classes = classNames('project-list', {
                    'page-top-margin': grid.set !== 'projects'
                });

                let fade = classNames({
                    'page-top-margin': grid.set !== 'projects'
                });

                let hero = (grid.set === 'projects') ? <ProjectHero {...this.state} /> : <span />;

                return (
                    /* jshint ignore:start */
                    <div className="page">
                        {hero}
                        <div className={fade}>
                            {this.getMask()}
                            <div ref="grid" className={classes}>
                                <ImageGrid data={grid} />
                            </div>
                        </div>
                        {showNextPrev}
                    </div>
                    /* jshint ignore:end */
                );
            }

        } else {
            return <Loading />;
        }
    }
});

module.exports = Project;
