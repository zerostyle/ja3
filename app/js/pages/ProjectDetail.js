var Reflux = require('reflux');
var React = require('react');
var Router = require('react-router');
var _ = require('lodash');
var LoadEventMixin = require('../mixins/LoadEventMixin.js');
var Img = require('../components/Img.js');
var Carousel = require('../components/Carousel.js');
var CarouselButton = require('../components/CarouselButton.js');
var Loading = require('../components/Loading.js');
var SecretMixin = require('../mixins/SecretMixin.js');

var ProjectDetail = React.createClass({
	mixins: [LoadEventMixin, Reflux.ListenerMixin, Router.Navigation, SecretMixin],

	componentWillMount: function() {
		this.setState({num:Number(this.props.params.num)});
	},

	componentWillReceiveProps: function(nextProps){
        this.setState({num:Number(nextProps.params.num)});
    },

	render: function() {
		
		if (this.state && this.state.loaded) {
			var image = {src: this.state.images[this.state.num - 1].lrg, className:'img-detail-lrg'};
			var nextNum = Number(this.state.num) + 1;
			if (nextNum >= this.state.images.length) nextNum = 0;

			var path = this.props.data.id.split('/')[0];

			if (this.state.showPassword && this.state.secret) {
				return this.secretInput();
			} else {

				return (
					/* jshint ignore:start */
					<div className="project-detail">
						<Carousel images={this.state.images} path={this.props.data.id} num={this.state.num} />
					</div>
					/* jshint ignore:end */
				);
			}

		} else {
			return <Loading />;
		}
		
	}
});

module.exports = ProjectDetail;

/*

<Router.Link to={path + 'Detail'} params={{id: this.state.id, num:nextNum}}>
	<Img key={this.state.num} data={image} />
</Router.Link>

*/