import Reflux from 'reflux';
import React from 'react';
import Router from 'react-router';
import TransitionMixin from '../mixins/TransitionMixin.jsx';
import LoadEventMixin from '../mixins/LoadEventMixin.js';
import ProjectsGrid from '../components/ProjectsGrid.js';
import Loading from '../components/Loading.js';

let Projects = React.createClass({
    mixins: [LoadEventMixin, Router.State, Reflux.ListenerMixin, TransitionMixin],

    componentDidMount() {
        document.body.scrollTop = 0;
    },

    render() {
        if (this.state && this.state.loaded) {
            return (
                /* jshint ignore:start */
                <div className="page page-top-margin">
                    {this.getMask()}
                    <ProjectsGrid sets={this.state.sets} classList="projects" />
                </div>
                /* jshint ignore:end */
            );
        } else {
            return <Loading />;
        }
    }
});

module.exports = Projects;
