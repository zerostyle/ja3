import Reflux from 'reflux';
import React from 'react';
import Router from 'react-router';
import last from 'lodash/array/last';
import LoadEventMixin from '../mixins/LoadEventMixin.js';
import ImageStory from '../components/ImageStory.jsx';
import BackgroundImg from '../components/BackgroundImg.jsx';
import Loading from '../components/Loading.js';
import SecretMixin from '../mixins/SecretMixin.js';
import Draggable from '../lib/greensock/utils/Draggable.js';

let Story = React.createClass({
    mixins: [LoadEventMixin, Reflux.ListenerMixin, SecretMixin, Router.Navigation],

    getInitialState() {
        return {
            checked: true,
            scrollWidth: 0
        };
    },

    componentDidMount() {
        this.startDrag();
    },

    componentDidUpdate() {
        this.startDrag();
    },

    startDrag(){
        if (this.state && this.state.loaded && this.state.images) {

            let scrollWidth = 0;
            this.state.images.forEach((item, i) => {
                let img = React.findDOMNode(this.refs['img' + i]);
                console.log(img.clientWidth);
                scrollWidth += img.clientWidth;
            });

             console.log(scrollWidth, this.refs);

            //this.setState({scrollWidth});

            let el = React.findDOMNode(this.refs.drag);
            let bounds = React.findDOMNode(this.refs.bounds);

            Draggable.create(el, {
                bounds: bounds,
                type: 'x',
                edgeResistance: 1,
                throwProps: true,
                minimumMovement: 6,
                onDrag: this.onDrag
            });
        }
    },

    onDrag() {

    },

    closeView() {
        //let paths = this.props.path.split('/');
        //this.transitionTo(paths[0] + 'List', {id:paths[1]});
    },

    goToHome() {
        //this.transitionTo('home');
    },

    render() {

        if (this.state && this.state.loaded && this.state.images) {
            let paths = this.props.data.id.split('/');

            let grid = {
                set: paths[0],
                setID: last(paths),
                images: this.state.images
            };

            if (this.state.showPassword && this.state.secret) {
                return this.secretInput();
            } else {

                let getItem = (item, i) => {
                    return (
                        /* jshint ignore:start */
                        <div key={i} className="fs-story">
                            <img ref={'img' + i} src={item.lrg} />
                        </div>
                        /* jshint ignore:end */
                    );
                };

                return (
                    /* jshint ignore:start */
                   <div ref="bounds" className="story project-detail">

                        <div className="fs fade-in2">
                            <div className="fs-nav">

                                <div className="fs-nav-row fs-nav-high">

                                    <div className="fs-btn fs-mark" onClick={this.goToHome}>
                                        <svg className="ja-grey" x="0px" y="0px" viewBox="-289 385 32 23">
                                           <polygon className="ja-grey-shape" points="-271,385 -271,385 -271,385 -279.9,400.2 -279.9,400.2 -288.9,400.2 -284.4,407.9 -275.5,407.9 -271,400.3 -266.5,407.9 -257.6,407.9 " />
                                        </svg>
                                    </div>

                                    <div className="fs-btn fs-close " onClick={this.closeView}>
                                        <svg className="close-svg" width="22px" height="22px" viewBox="0 0 22 22">
                                            <g transform="translate(1.000000, 1.000000)">
                                                <path className="close-line" d="M0.347826087,19.5454545 L19.7391304,0.454545455" />
                                                <path className="close-line" d="M19.6521739,19.5454545 L0.260869565,0.454545455" />
                                            </g>
                                        </svg>
                                    </div>
                                </div>
                                <div ref="bounds" className="fs drag-container" style={{width: this.state.scrollWidth}}>
                                    <div ref="drag" className="drag" >
                                       {this.state.images.map(getItem)}
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                    /* jshint ignore:end */
                );
            }
        }

        return <Loading />;
    }
});

module.exports = Story;
