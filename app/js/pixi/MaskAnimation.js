import chunk from 'lodash/array/chunk';
import Pixi from'pixi.js';
import PixiImageMask from './PixiImageMask.js';
import RatioUtil from '../utils/RatioUtil.js';
import HomeActions from '../actions/HomeActions.js';
import load from '../utils/imageLoader.js';
import siteURL from '../utils/siteURL.js';
import isUndefined from 'lodash/lang/isUndefined';

class MaskAnimation {

    constructor(options) {
        this.container = options.container;
        this.div = options.div;
        this.size = {width: 514, height: 300};
        this.maskImages = options.images.filter((img) => { return img.mask !== ''; });
        this.imagePosition = -1;

        this.ratioUtil = new RatioUtil();
        this.sprite = new PIXI.Sprite();
        this.container.addChild(this.sprite);

        this.imgContainer = new PIXI.Sprite();
        this.sprite.addChild(this.imgContainer);

        // Images
        let maskArray = [];
        let loadArray = [siteURL + '/assets/img/ja-blue.png'];
        let prev = null;
        this.maskImages.forEach(item => {
            let m = new PixiImageMask({container: this.imgContainer, image: item.lrg, maskImage: siteURL + '/assets/img/ja-blue.png', size: this.size, prev: prev});
            maskArray.push(m);
            prev = m.getSprite();
            loadArray.push(load(item.lrg));
        });

        let chunkArr = chunk(maskArray, 2);
        this.firstChunk = chunkArr[0];
        this.lastChunk = chunkArr[1];

        HomeActions.loading(1, 'pixi');

        Promise.all(loadArray)
        .then(
           this.onReady.bind(this)
        );

        this.tl = new TimelineMax();
    }

    onReady() {
        HomeActions.ready(1, 'pixi');
    }

    componentWillUnmount() {
        this.tl.kill();
        window.removeEventListener('resize', this.handleResize);
    }

    transitionIn() {
        this.tl.clear();
        this.tl.to(this.imgContainer, 0, {alpha: 1, ease: Quad.easeInOut});
        this.firstChunk.forEach(item => { item.fadeIn(this.tl, null); });
        if (!isUndefined(this.lastChunk)) this.lastChunk.forEach(item => { item.fadeIn(this.tl, this.firstChunk[this.firstChunk.length - 1]); });
        this.tl.to(this.imgContainer, 1, {alpha: 0, ease: Quad.easeInOut});
        this.tl.addCallback(this.animationComplete.bind(this));

        // Resize
        this.handleResize();
        window.addEventListener('resize', this.handleResize.bind(this));
    }

    animationComplete() {
        HomeActions.animationComplete(0, 'pixi');
    }

    scaleToFit(target, size, bounds, scalePercent) {
        let rect = this.ratioUtil.scaleToFit(size, bounds);
        let percent = rect.width / size.width;

        target.width = (scalePercent) ? percent : rect.width;
        target.height = (scalePercent) ? percent : rect.height;

        return rect;
    }

    handleResize() {
        this.viewWidth = document.body.clientWidth;
        this.viewHeight = this.div.offsetHeight;

        let maxH = 400;
        this.offsetY = 0;
        if (this.viewWidth > 320) {
            if (this.viewHeight < 650) {
                maxH = 300;
            }
            if (this.viewHeight < 550) {
                maxH = 250;
                this.offsetY = 6;
            }
            if (this.viewHeight < 450) {
                maxH = 200;
                this.offsetY = 12;
            }
            if (this.viewHeight < 400) {
                maxH = 150;
                this.offsetY = 18;
            }
        }

        let padding = 60;
        let boundsWidth = this.viewWidth - padding;
        let boundsHeight = this.viewHeight - padding;
        let maxWidth = (boundsWidth > 700) ? 700 : boundsWidth;
        let maxHeight = (boundsHeight > maxH) ? maxH : boundsHeight;
        let bounds = {width: maxWidth, height: maxHeight};

        let rect = this.scaleToFit(this.sprite, this.size, bounds, true);

        this.sprite.position.x = (this.viewWidth - rect.width) * 0.5;
        this.sprite.position.y = (this.viewHeight - rect.height) * 0.5 + this.offsetY;
    }

    destroy() {
        this.tl.kill();
        this.container.removeChild(this.sprite);
        this.sprite.destroy(true);
        window.removeEventListener('resize', this.handleResize);
    }

}

export default MaskAnimation;
