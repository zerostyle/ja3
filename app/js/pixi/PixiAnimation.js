import random from 'lodash/number/random';
import Pixi from'pixi.js';
import RatioUtil from '../utils/RatioUtil.js';
import HomeActions from '../actions/HomeActions.js';
import load from '../utils/imageLoader.js';
import siteURL from '../utils/siteURL.js';

class PixiAnimation {

    constructor(options) {
        this.container = options.container;
        this.div = options.div;
        this.size = {width: 514, height: 300};

        let isRed = random(0, 1);
        this.triColor = (isRed === 0) ? 'red' : 'blue';

        this.ratioUtil = new RatioUtil();
        this.sprite = new PIXI.Sprite();
        this.container.addChild(this.sprite);

        this.triangleContainer = new PIXI.Sprite();
        this.trianglesArray = this.createShape(this.triangleContainer, true);
        TweenMax.set(this.trianglesArray, {alpha: 0});
        this.sprite.addChild(this.triangleContainer);

        let imgTex = PIXI.Texture.fromImage(siteURL + '/assets/img/ja-' + this.triColor + '.png');
        this.ja = new PIXI.Sprite(imgTex);
        this.ja.width = 500;
        this.ja.height = 372;
        this.ja.alpha = 0;
        this.triangleContainer.addChild(this.ja);

        // Main TimelineMax
        this.tl = new TimelineMax();

        HomeActions.loading(0, 'pixi');

        Promise.all([
            load(siteURL + '/assets/img/ja-' + this.triColor + '.png'),
            load(siteURL + '/assets/img/' + this.triColor + '-up.png'),
            load(siteURL + '/assets/img/' + this.triColor + '-down.png')
        ]).then(
           this.onReady.bind(this)
        );
    }

    onReady() {
        HomeActions.ready(0, 'pixi');
    }

    componentWillUnmount() {
        this.tl.kill();
        window.removeEventListener('resize', this.handleResize);
    }

    transitionIn() {
        this.tl.clear();
        this.tl.staggerTo( this.trianglesArray, 0.5, {alpha: 1, ease: Quad.easeInOut}, 0.1, 'in');
        this.tl.to(this.ja, 0.5, {alpha: 1, ease: Quad.easeIn}, 'in+=0.6');
        this.tl.to(this.sprite, 5, {});
        this.tl.addCallback(this.transitionOut.bind(this));

        this.handleResize();
        window.addEventListener('resize', this.handleResize.bind(this));
    }

    transitionOut() {
        this.tl.to(this.ja, 0.5, {alpha: 0, ease: Quad.easeOut}, 'out');
        this.tl.staggerTo(this.trianglesArray, 0.5, {alpha: 0, ease: Quad.easeInOut}, 0.1, 'out');
        this.tl.to(this.sprite, 0.5, {});
        this.tl.addCallback(this.animationComplete.bind(this));
    }

    animationComplete() {
        HomeActions.animationComplete(0, 'pixi');
    }

    createShape(sprite, red) {
        let upArrow = PIXI.Texture.fromImage(siteURL + '/assets/img/' + this.triColor + '-up.png');
        let downArrow = PIXI.Texture.fromImage(siteURL + '/assets/img/' + this.triColor + '-down.png');

        let triW = 143;
        let triH = 124;

        this.jaWidth = triW * 6;
        this.jaHeight = triH * 4;

        let s9 = new PIXI.Sprite(upArrow);
        s9.x = triW * 2.5;
        s9.y = triH * 2;
        sprite.addChild(s9);

        let s8 = new PIXI.Sprite(downArrow);
        s8.x = triW * 2;
        s8.y = triH * 2;
        sprite.addChild(s8);

        let s7 = new PIXI.Sprite(upArrow);
        s7.x = triW * 2;
        s7.y = triH;
        sprite.addChild(s7);

        let s6 = new PIXI.Sprite(upArrow);
        s6.x = triW * 1.5;
        sprite.addChild(s6);

        let s5 = new PIXI.Sprite(downArrow);
        s5.x = triW * 1.5;
        s5.y = triH;
        sprite.addChild(s5);

        let s4 = new PIXI.Sprite(upArrow);
        s4.x = triW;
        s4.y = triH;
        sprite.addChild(s4);

        let s3 = new PIXI.Sprite(downArrow);
        s3.x = triW;
        s3.y = triH * 2;
        sprite.addChild(s3);

        let s2 = new PIXI.Sprite(upArrow);
        s2.x = triW * 0.5;
        s2.y = triH * 2;
        sprite.addChild(s2);

        let s1 = new PIXI.Sprite(downArrow);
        s1.y = triH * 2;
        sprite.addChild(s1);

        return [s1, s2, s3, s4, s5, s6, s7, s8, s9];
    }

    scaleToFit(target, size, bounds, scalePercent) {
        let rect = this.ratioUtil.scaleToFit(size, bounds);
        let percent = rect.width / size.width;

        target.width = (scalePercent) ? percent : rect.width;
        target.height = (scalePercent) ? percent : rect.height;

        return rect;
    }

    handleResize() {
        this.viewWidth = document.body.clientWidth;
        this.viewHeight = this.div.offsetHeight;

        let maxH = 400;
        this.offsetY = 0;
        if (this.viewWidth > 320) {
            if (this.viewHeight < 650) {
                maxH = 300;
            }
            if (this.viewHeight < 550) {
                maxH = 250;
                this.offsetY = 6;
            }
            if (this.viewHeight < 450) {
                maxH = 200;
                this.offsetY = 12;
            }
            if (this.viewHeight < 400) {
                maxH = 150;
                this.offsetY = 18;
            }
        }

        let padding = 60;
        let boundsWidth = this.viewWidth - padding;
        let boundsHeight = this.viewHeight - padding;
        let maxWidth = (boundsWidth > 700) ? 700 : boundsWidth;
        let maxHeight = (boundsHeight > maxH) ? maxH : boundsHeight;
        let bounds = {width: maxWidth, height: maxHeight};

        let rect = this.scaleToFit(this.sprite, this.size, bounds, true);

        this.sprite.position.x = (this.viewWidth - rect.width) * 0.5;
        this.sprite.position.y = (this.viewHeight - rect.height) * 0.5 + this.offsetY;
    }

    destroy() {
        this.tl.kill();
        this.container.removeChild(this.sprite);
        this.sprite.destroy(true);
        window.removeEventListener('resize', this.handleResize);
    }

}

export default PixiAnimation;
