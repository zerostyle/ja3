import random from 'lodash/number/random';
import isNull from 'lodash/lang/isNull';
import Pixi from'pixi.js';
import RatioUtil from '../utils/RatioUtil.js';

class PixiImageMask {

    constructor(options) {
        this.container = options.container;
        this.ratioUtil = new RatioUtil();
        this.size = options.size;
        this.prevDisplayObject = options.prev;
        this.offsetY = 0;
        this.id = 'id' + random(0, 10000);

        let maskTexture = PIXI.Texture.fromImage(options.maskImage);
        this.imageMask = new PIXI.Sprite(maskTexture);
        this.imageMask.alpha = 2.2;
        this.container.addChild(this.imageMask);

        let imgTex = PIXI.Texture.fromImage(options.image);
        this.sprite = new PIXI.Sprite(imgTex);
        this.sprite.mask = this.imageMask;
        this.sprite.width = 500;
        this.sprite.height = 372;
        this.sprite.alpha = 0;
        this.container.addChild(this.sprite);
    }

    fadeIn(tl) {
        let time = 1;
        this.sprite.alpha = 0;
        tl.to(this.sprite, time, {alpha: 1, ease: Quad.easeInOut}, this.id);
        if (!isNull(this.prevDisplayObject)) tl.to(this.prevDisplayObject, time, {alpha: 0, ease: Quad.easeInOut, delay: time * 0.5}, this.id);
        tl.to(this.sprite, time, {alpha: 1, delay: 1});

        this.container.addChild(this.sprite);
    }

    getSprite() {
        return this.sprite;
    }

    destroy() {
        this.container.removeChild(this.sprite);
        this.sprite.destroy(true);
    }

}

export default PixiImageMask;
