class PixiJa {

  constructor(options) {

    this.container = options.container;

    let seg = 70;
    let seg2 = seg * 2;
    let seg3 = seg * 3;
    let seg31 = seg * 3.5;
    let seg4 = seg * 4;
    let seg5 = seg * 5;
    let seg6 = seg * 7;

    this.graphics = new PIXI.Graphics();
    this.graphics.beginFill(0x4FBBEB);
    this.graphics.lineStyle(1, 0x4FBBEB, 1);
    this.graphics.moveTo(0, seg31);
    this.graphics.lineTo(seg, seg5);
    this.graphics.lineTo(seg3, seg5);
    this.graphics.lineTo(seg4, seg31);
    this.graphics.lineTo(seg5, seg5);
    this.graphics.lineTo(seg6, seg5);
    this.graphics.lineTo(seg4, 0);
    this.graphics.lineTo(seg2, seg31);
    this.graphics.endFill();
    this.container.addChild(this.graphics);
  }

  getPixiElement(){
    return this.graphics;
  }

  destroy() {
    this.container.removeChild(this.graphics);
  }

}

export default PixiJa;