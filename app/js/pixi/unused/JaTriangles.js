import Pixi from'pixi.js';
import siteURL from '../utils/siteURL.js';

class JaTriangles {
	
	createTriangles(sprite) {
		let upArrow = PIXI.Texture.fromImage(siteURL + '/assets/img/blueUP.png');
		let downArrow = PIXI.Texture.fromImage(siteURL + '/assets/img/blueDarkDown.png');

		let triW = 143;
		let triH = 124;
		
		this.jaWidth = triW * 6;
		this.jaHeight = triH * 4;

		let s9 = new PIXI.Sprite(upArrow);
		s9.x = triW * 2.5;
		s9.y = triH * 2;
		sprite.addChild(s9);

		let s8 = new PIXI.Sprite(downArrow);
		s8.x = triW * 2;
		s8.y = triH * 2;
		sprite.addChild(s8);

		let s7 = new PIXI.Sprite(upArrow);
		s7.x = triW * 2;
		s7.y = triH;
		sprite.addChild(s7);
		
		let s6 = new PIXI.Sprite(upArrow);
		s6.x = triW * 1.5;
		sprite.addChild(s6);

		let s5 = new PIXI.Sprite(downArrow);
		s5.x = triW * 1.5;
		s5.y = triH;
		sprite.addChild(s5);

		let s4 = new PIXI.Sprite(upArrow);
		s4.x = triW;
		s4.y = triH;
		sprite.addChild(s4);

		let s3 = new PIXI.Sprite(downArrow);
		s3.x = triW;
		s3.y = triH * 2;
		sprite.addChild(s3);

		let s2 = new PIXI.Sprite(upArrow);
		s2.x = triW * 0.5;
		s2.y = triH * 2;
		sprite.addChild(s2);

		let s1 = new PIXI.Sprite(downArrow);
		s1.y = triH * 2;
		sprite.addChild(s1);

		return [s1, s2, s3, s4, s5, s6, s7, s8, s9];
	}

}

export default JaTriangles;