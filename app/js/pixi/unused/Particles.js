import pixiParticles from '../lib/pixi-particles.js';

class Particles {

    constructor(options) {
        this.container = options.container;
        this.type = options.type;
        this.emitter = null;
        this.elapsed = Date.now();

        this.config = {
            'alpha': {
                'start': 1,
                'end': 0
            },
            'scale': {
                'start': 0.1,
                'end': 2.5,
                'minimumScaleMultiplier': 0.1
            },
            'color': {
                'start': '#FFFFFF',
                'end': '#FFFFFF'
            },
            'speed': {
                'start': 100,
                'end': 500
            },
            'acceleration': {
                'x': 10,
                'y': 10
            },
            'startRotation': {
                'min': 0,
                'max': 360
            },
            'rotationSpeed': {
                'min': 0,
                'max': 50
            },
            'lifetime': {
                'min': 0.1,
                'max': 3
            },
            'blendMode': 'normal',
            'frequency': 0.03,
            'emitterLifetime': -1,
            'maxParticles': 500,
            'pos': {
                'x': 0,
                'y': 0
            },
            'addAtBack': false,
            'spawnType': 'circle',
            'spawnCircle': {
                'x': 0,
                'y': 0,
                'r': 1000
            }
        };

        this.assetArray = [
            '/assets/img/grey-up.png',
            '/assets/img/grey-down.png',
            '/assets/img/white-up.png'
        ];

        // var loader = PIXI.loader; // pixi exposes a premade instance for you to use.
        // loader.add('up', '/assets/img/grey-up.png');
        // loader.add('down', '/assets/img/grey-down.png');
        // loader.once('complete', this.onComplete.bind(this));
        // loader.load();
        // }

        // onComplete() {
            
        let textures = [];
        for (let i = 0; i < this.assetArray.length; ++i) {
            textures.push( PIXI.Texture.fromImage(this.assetArray[i]) );
        }

        // Create the new emitter and attach it to the stage
        let emitterContainer = new PIXI.Container();
        this.container.addChild(emitterContainer);
        this.emitter = new cloudkid.Emitter( emitterContainer, textures, this.config );

        // Click on the canvas to trigger
        // canvas.addEventListener('mouseup', function(e){
        //  emitter.emit = true;
        //  emitter.resetPositionTracking();
        //  emitter.updateOwnerPos(e.offsetX || e.layerX, e.offsetY || e.layerY);
        // });

        let blurFilter = new PIXI.filters.BlurFilter();
        blurFilter.blur = 4;
        emitterContainer.filters = [blurFilter];

        let tl = new TimelineMax({repeat: -1, yoyo: false, repeatDelay: 1, delay: 1});
        tl.to(blurFilter, 2, {blur: 2, ease:Quad.easeInOut });
        tl.to(blurFilter, 2, {blur: 100, ease:Quad.easeInOut });
        tl.to(blurFilter, 2, {blur: 2, ease:Quad.easeInOut });
        tl.to(blurFilter, 4, {blur: 100, ease:Quad.easeInOut });
        tl.to(blurFilter, 1, {blur: 2, ease:Quad.easeInOut });

        // Start the update
        this.animate();
    }

    resize(w, h) {
        if (this.emitter) this.emitter.updateOwnerPos(w * 0.5, h * 0.5);
    }

    animate() {
        let now = Date.now();
        if (this.emitter) this.emitter.update((now - this.elapsed) * 0.001);
        this.elapsed = now;
    }
}

export default Particles;
