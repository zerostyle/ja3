import random from 'lodash/number/random';
import Pixi from'pixi.js';
import RatioUtil from '../utils/RatioUtil.js';

class PixiImage {

    constructor(options) {
        this.container = options.container;
        this.ratioUtil = new RatioUtil();
        this.size = options.size;
        this.offsetY = 0;
        this.id = 'id' + random(0, 10000);

        let imgTex = PIXI.Texture.fromImage(options.image);
        this.sprite = new PIXI.Sprite(imgTex);
        this.sprite.width  = 500;
        this.sprite.height = 372;
        // this.sprite.alpha = 0;
        // this.container.addChild(this.sprite);
    }

    getSprite() {
        return this.sprite;
    }

    destroy() {
        this.container.removeChild(this.sprite);
        this.sprite.destroy(true);
    }

}

export default PixiImage;
