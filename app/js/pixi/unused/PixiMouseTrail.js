import random from 'lodash/number/random';

class PixiMouseTrail {

    constructor(options) {

        this.container = options.container;
        this.shapes = [];
        this.mouseX = 0;
        this.mouseY = 0,
        this.shapeInterval = null;
        this.nextShape = 0;

        let textures = [
            PIXI.Texture.fromImage('/assets/img/grey-up.png'),
            PIXI.Texture.fromImage('/assets/img/grey-up.png'),
            PIXI.Texture.fromImage('/assets/img/grey-down.png')
        ];

        //create triangles
        this.shapeSprite = new PIXI.Sprite();
        this.container.addChild(this.shapeSprite);

        for (let i = 0; i < 300; i++) {
            this.createShape(textures[i%3]);
        }

        let blurFilter = new PIXI.filters.BlurFilter();
        //blurFilter.blur = 6;
        //this.shapeSprite.filters = [blurFilter];

        let crossHatch = new PIXI.filters.CrossHatchFilter();
        crossHatch.crossHatch = 100;

        let pixelate = new PIXI.filters.PixelateFilter();
        //pixelate.size = 106;
        pixelate.padding = 6;
        this.shapeSprite.filters = [blurFilter];

        //TweenMax.to(pixelate, 10, {padding:100, ease:Quad.easeOut, repeat:-1, yoyo:true});
        //TweenMax.to(blurFilter, 5, {blur:100, ease:Quad.easeOut, repeat:-1, yoyo:true, repeatDelay:1});
    }

    start() {
        window.addEventListener('mousemove', this.updatePos.bind(this));
        //window.addEventListener('touchstart', this.updatePos);
        //window.addEventListener('touchmove', this.updatePos);
    }

    createShape(texture) {
        let sizes = [30, 40, 50, 60, 70, 80, 90, 100, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 150, 200, 400];
        let size = sizes[random(0, sizes.length-1)];
        let s = new PIXI.Sprite(texture);
        s.width = size + Math.random() * 30;
        s.height = size + Math.random() * 30;
        s.anchor.x = 0.5;
        s.anchor.y = 0.5;

        this.shapeSprite.addChild(s);
        s.alpha = 0;
        s.launched = false;
        this.shapes.push(s);
    }

    launchShape() {
        let s = this.shapes[this.nextShape];
        this.nextShape = (this.nextShape === this.shapes.length - 1) ? 0 : this.nextShape + 1;
        s.launched = true;
        s.alpha = 1;
        s.position.x = random(0, document.body.clientWidth); //this.mouseX;
        s.position.y = random(0, document.body.clientHeight); //this.mouseY;

        s.vx = -1 + Math.random()*2;
        s.vy = Math.random()*-1;
        s.vr = -0.2 + Math.random()*0.4;
        s.p = 0;
    }

    updatePos(e) {
        this.mouseX = e.pageX || e.changedTouches[0].pageX;
        this.mouseY = e.pageY || e.changedTouches[0].pageY;
    }

    animate() {
        this.launchShape();

        for (let i = 0; i < this.shapes.length; i++) {
            if(this.shapes[i].launched) {
              let angle = Math.PI * (1-this.shapes[i].p);
              this.shapes[i].rotation += this.shapes[i].vr * 0.1;
              this.shapes[i].position.x += 0.3 * Math.cos(angle) + this.shapes[i].vx;
              this.shapes[i].position.y -= 0.3 * Math.sin(angle) + this.shapes[i].vy;
              this.shapes[i].vy += 0.04;
              this.shapes[i].p += this.shapes[i].vr;
            }
        }
    }

    destroy() {
        window.removeEventListener('mousemove', this.updatePos);
        //window.removeEventListener('touchstart', this.updatePos);
        //window.removeEventListener('touchmove', this.updatePos);

        this.container.removeChild(this.shapeSprite);

    }

}

export default PixiMouseTrail;