import Reflux from 'reflux';
import shuffle from 'lodash/collection/shuffle';
import HomeActions from '../actions/HomeActions.js';
import isUndefined from 'lodash/lang/isUndefined';

let HomeStore = Reflux.createStore({

   init() {
        this.currentPosition = -1;
        this.assets = [];
        this.queue = [];
        this.init = false;
        this.on = false;
        this.lastAsset = null;
        this.pauseInSeconds = 5;
        this.assetsReady = 0;

        this.listenTo(HomeActions.on, this.turnOn);
        this.listenTo(HomeActions.off, this.turnOff);
        this.listenTo(HomeActions.loading, this.onAssetLoadStart);
        this.listenTo(HomeActions.ready, this.onAssetReady);
        this.listenTo(HomeActions.animationComplete, this.onAnimationComplete);
    },

    turnOn() {
        this.on = true;
        this.nextAsset();
    },

    turnOff() {
        this.on = false;
        this.init = false;

        this.stopListeningTo(HomeActions.loading, this.onAssetLoadStart);
    },

    onAssetLoadStart(id, type) {
        // console.log('onAssetLoadStart', id, type);
        let asset = {_id: type + id, id, type};
        this.assets.push(asset);
    },

    onAssetReady(id, type) {
        // console.log('onAssetLoadStart', id, type);
        this.assetsReady++;

        let asset;
        let _id = type + id;
        this.assets.forEach((item) => {
            if (item._id === _id) {
                asset = item;
                return;
            }
        });

        this.queue.push(asset);
        this.queue = shuffle(this.queue); // shuffle

        if (this.queue.length === 1 && !this.init) this.nextAsset();
        this.init = true;

        if (this.assetsReady === this.assets.length) {
            this.stopListeningTo(HomeActions.ready, this.onAssetReady);
        }
    },

    onAnimationComplete(id, type) {
        // console.log('onAnimationComplete', id, type);
        this.nextAsset();
    },

    nextAsset() {
        // console.log('nextAsset');

        if (this.on && this.assets.length) {
            if (!this.queue.length) {
                this.queue = shuffle(this.assets);
            }

            let asset = this.queue.shift();
            if (isUndefined(asset)) {
                return;
            } else if (this.lastAsset && (asset._id === this.lastAsset._id)) {
                // console.log('nextAsset unq');
                this.nextAsset();
                this.lastAsset = asset;
                return;
            } else {
                // console.log('nextAsset trigger', asset);
                this.trigger(asset);
            }
        }
    }
});

export default HomeStore;
