import Reflux from 'reflux';
import React from 'react';
import ImageQueueActions from '../actions/ImageQueueActions.js';

let ImageQueueStore = Reflux.createStore({
    maxRequests: 3,
    closedRequests: {},
    queueList: [],
    queueByHref: {},
    numOpenRequests: 0,
    openRequests: {},

    getInitialState() {
        // return this.data;
    },

    init(){
        // this.trigger(this.data);
        // Actions.imageLoaded();
    },

    addImage(href, onload) {
        var request = {href: href, onloaded: onload};

        this.queueList.push(request);
        this.queueByHref[request.href] = request;
    },

    cancelAllLoads(){
        this.closedRequests = {};
        this.queueList = [];
        this.queueByHref = {};
        this.numOpenRequests = 0;
        this.openRequests = {};
    },

    cancelLoad(href) {
        // If the request is open, close it.

        if (href in this.openRequests) {
            // If the image is not yet loaded, prevent it from loading.

            if (this.closedRequests[href] === undefined) {
                this.openRequests[href].img.onload = function() {};
                this.openRequests[href].img.onerror = function() {};
                this.openRequests[href].img.src = "about:";
                this.openRequests[href].img = null;
            }

            delete this.openRequests[href];
            this.numOpenRequests--;
        }

        // Mark the image null in the queue so it will be skipped.

        // get the href out of the queue
        if(href in this.queueByHref) delete this.queueByHref[href];
    },

    prioritizeQueue(pattern) {
        var sortFunction = function(r1, r2) {
            return Number(Boolean(r2.href.match(pattern))) - Number(Boolean(r1.href.match(pattern)));
        };

        this.queueList.sort(sortFunction);
    },

    reverseQueue() {
        this.queueList.reverse();
    },

   /**
    * Request up to maxRequests:3 images from the queue, skipping blank items.
    */
    processQueue() {
        //console.log(this.queueByHref, this.queueList);

        while(this.numOpenRequests < this.maxRequests && this.queueList.length > 0) {
            var href = this.queueList.shift().href;

            //console.log(this.numOpenRequests, this.queueList.length);

            if(href in this.queueByHref) {
                this.loadImage(this.queueByHref[href]);
                this.openRequests[href] = this.queueByHref[href];
                delete this.queueByHref[href];
                this.numOpenRequests++;
            }
        }
    },

    loadImage(request) {

        request.img = new Image();

        request.img.onload =() => {
            request.onloaded(undefined, request.img);
            this.closedRequests[request.href] = Date.now();
            this.cancelLoad(request.href);

            this.trigger(request.img);
            //ImageQueueActions.imageLoaded(request.img);
        };

        request.img.onerror = function(error) {
            request.onloaded(error, request.img);
            this.closedRequests[request.href] = Date.now();
            this.cancelLoad(request.href);
        };

        request.img.src = request.href;
    },

    queueState() {
        return [this.numOpenRequests, this.queueList.length];
    }

});

module.exports = ImageQueueStore;
