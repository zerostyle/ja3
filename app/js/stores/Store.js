import Reflux from 'reflux';
import Axios from 'axios';
import keys from 'lodash/object/keys';
import indexOf from 'lodash/array/indexOf';
import merge from 'lodash/object/merge';
import clone from 'lodash/lang/clone';

let Store = Reflux.createStore({

    data: {
        loaded: false,
        clientsLoaded: false
    },

    init() {
        let url = location.origin;
        if (url === 'http://localhost:3000')
            url += '/assets/json/json-app.json';
        else
            url += '/site/json-app';

        Axios.get(url)
        .then(response => {
            this.data = clone(response.data, true);
            this.data.clientsLoaded = false;
            this.data.loaded = true;

            this.projects = keys(this.data.pages.projects.sets);
            this.images = keys(this.data.pages.images.sets);

            this.homeSets = merge(response.data.pages.images, response.data.pages.projects);


            this.trigger(this.data);
        })
        .then(this.loadClientSets)
        .catch(error => console.warn(error));
    },

    loadClientSets() {
        let url = location.origin;
        if (url === 'http://localhost:3000')
            url += '/assets/json/json-c.json';
        else
            url += '/site/json-c';

        Axios.get(url)
        .then(response => {
            this.data.pages.clients = clone(response.data, true);
            this.data.clientsLoaded = true;

            this.trigger(this.data);
        })
        .catch(error => console.warn(error));
    },

    getSet(path, id) {
        return this.data.pages[path].sets[id];
    },

    getPrevNextProject(path, id) {
        let set = this[path];

        let index = indexOf(set, id);
        let len = set.length - 1;

        let previousNum = index - 1;
        if (previousNum < 0) previousNum = len;

        let nextNum = index + 1;
        if (nextNum > len) nextNum = 0;

        return { prev: this.getSet(path, set[previousNum]), next: this.getSet(path, set[nextNum]), path: path + 'List'};
    }
});

module.exports = Store;
