import clone from 'lodash/lang/clone';

class RatioUtil {

    /**
        Determines the ratio of width to height.

        @param rect: The area's width and height expressed as a object, with x and y properties.
    */
    widthToHeight(rect) {
        return rect.width / rect.height;
    }

    /**
        Determines the ratio of height to width.

        @param rect: The area's width and height expressed as an object, with x and y properties.
    */
    heightToWidth(rect) {
        return rect.height / rect.width;
    }

    /**
        Scales the width of an area while preserving aspect ratio.

        @param rect: The area's width and height expressed as a object, with x and y properties.
        @param height: The new height of the area.
        @param snapToPixel: Force the scale to whole pixels true, or allow sub-pixels false.
    */
    scaleWidth(rect, height, snapToPixel = true) {
        return this._defineRect(rect, height * this.widthToHeight(rect), height, snapToPixel);
    }

    /**
        Scales the height of an area while preserving aspect ratio.

        @param rect: The area's width and height expressed as a object, with x and y properties.
        @param width: The new width of the area.
        @param snapToPixel: Force the scale to whole pixels true, or allow sub-pixels false.
    */
    scaleHeight(rect, width, snapToPixel = true) {
        return this._defineRect(rect, width, width * this.heightToWidth(rect), snapToPixel);
    }

    /**
        Scales an area's width and height while preserving aspect ratio.

        @param size: The area's width and height expressed as a object, with x and y properties.
        @param amount: The amount you wish to scale by.
        @param snapToPixel: Force the scale to whole pixels true, or allow sub-pixels false.
    */
    scale(size, amount, snapToPixel = true) {
        let percent = parseFloat(amount) / 100.0;
        return this._defineRect(size, size.width * percent, size.height * percent, snapToPixel);
    }

    /**
        Resizes an area to fill the bounding area while preserving aspect ratio.

        @param rect: The area's width and height expressed as a object, with x and y properties.
        @param bounds: The area to fill. The Rectangle's x and y values are ignored.
        @param snapToPixel: Force the scale to whole pixels true, or allow sub-pixels false.
    */
    scaleToFill(rect, bounds, snapToPixel = true) {
        let scaled = this.scaleHeight(rect, bounds.width, snapToPixel);

        if (scaled.height < bounds.height) scaled = this.scaleWidth(rect, bounds.height, snapToPixel);

        return scaled;
    }

    /**
        Resizes an area to the maximum rect of a bounding area without exceeding while preserving aspect ratio.

        @param rect: The area's width and height expressed as a object, with x and y properties.
        @param bounds: The area the rectangle needs to fit within.
        @param snapToPixel: Force the scale to whole pixels true, or allow sub-pixels false.
    */
    scaleToFit(rect, bounds, snapToPixel = true) {
        let scaled = this.scaleHeight(rect, bounds.width, snapToPixel);

        if (scaled.height > bounds.height) scaled = this.scaleWidth(rect, bounds.height, snapToPixel);

        return scaled;
    }

    _defineRect(rect, width, height, snapToPixel) {
        let scaled = clone(rect);
        scaled.width = snapToPixel ? Math.round(width) : width;
        scaled.height = snapToPixel ? Math.round(height) : height;

        return scaled;
    }
}

export default RatioUtil;