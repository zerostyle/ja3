export default () => {
    let path = (location.hostname === 'localhost') ? location.hash.replace('#/', '') : location.pathname.replace('/', '');
	return path.replace('set/', '');
};